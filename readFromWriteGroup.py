"""
readFromWriteGroup
Read node generator adjusted for Avalon

Accepts Avalon Groups as write nodes. This is made to make it faster for artist to review
the render before publish

inspired by https://github.com/fredrikaverpil/nuke-scripts/blob/master/scripts/readFromWrite.py
"""

import nuke
import os, re
import glob

SINGLE_FILE_FORMATS = ['avi', 'mp4', 'mxf', 'mov', 'mpg', 'mpeg', 'wmv', 'm4v',
                       'm2v']

def evaluate_filepath_new(k_value, k_eval, project_dir):
    # get combined relative path
    combined_relative_path = None
    if k_eval is not None and project_dir is not None:
        combined_relative_path = os.path.abspath(os.path.join(project_dir, k_eval))
        combined_relative_path = combined_relative_path.replace('\\', '/')
        filetype = combined_relative_path.split('.')[-1]
        frame_number = re.findall(r'\d+', combined_relative_path)[-1]
        basename = combined_relative_path[: combined_relative_path.rfind(frame_number)]
        filepath_glob = basename + '*' + filetype
        glob_search_results = glob.glob(filepath_glob)
        if len(glob_search_results) <= 0:
            combined_relative_path = None

    if os.path.exists(k_value):
        filepath = k_value
    elif os.path.exists(k_eval):
        filepath = k_eval
    elif not isinstance(project_dir, type(None)) and \
            not isinstance(combined_relative_path, type(None)):
        filepath = combined_relative_path

    filepath = os.path.abspath(filepath)
    filepath = filepath.replace('\\', '/')
    current_frame = re.findall(r'\d+', filepath)[-1]
    padding = len(current_frame)
    basename = filepath[: filepath.rfind(current_frame)]
    filetype = filepath.split('.')[-1]

    # sequence or not?
    ismovie = False
    if filetype in SINGLE_FILE_FORMATS:
        ismovie = True
    else:
        # Image sequence needs hashes
        filepath = basename + '#'*padding + '.' + filetype
        ismovie = False

    # relative path? make it relative again
    if not isinstance(project_dir, type(None)):
        filepath = filepath.replace(project_dir, '.')

    # get first and last frame from disk
    frames = []
    firstframe = 0
    lastframe = 0
    filepath_glob = basename + '*' + filetype
    glob_search_results = glob.glob(filepath_glob)
    for f in glob_search_results:
        frame = re.findall(r'\d+', f)[-1]
        frames.append(frame)
    frames = sorted(frames)
    firstframe = frames[0]
    lastframe = frames[len(frames)-1]
    if lastframe < 0:
        lastframe = firstframe

    return filepath, firstframe, lastframe, ismovie


def create_read_node(ndata, comp_start):
    read = nuke.createNode('Read', 'file ' + ndata['filepath'])
    read.knob('colorspace').setValue(int(ndata['colorspace']))
    read.knob('raw').setValue(ndata['rawdata'])

    if ndata['ismovie']:
        read.knob('frame_mode').setValue("1")
        read.knob('frame').setValue(str(comp_start))
    elif comp_start == int(ndata['firstframe']):
        read.knob('frame_mode').setValue("1")
        read.knob('frame').setValue(str(comp_start))
        read.knob('first').setValue(int(ndata['firstframe']))
        read.knob('last').setValue(int(ndata['lastframe']))
        read.knob('origfirst').setValue(int(ndata['firstframe']))
        read.knob('origlast').setValue(int(ndata['lastframe']))
    else:
        read.knob('frame_mode').setValue("0")
        read.knob('first').setValue(int(ndata['firstframe']))
        read.knob('last').setValue(int(ndata['lastframe']))
        read.knob('origfirst').setValue(int(ndata['firstframe']))
        read.knob('origlast').setValue(int(ndata['lastframe']))
    read.knob('xpos').setValue(ndata['new_xpos'])
    read.knob('ypos').setValue(ndata['new_ypos'])
    nuke.inputs(read, 0)

    return


def readFromWrite():

    comp_start = nuke.Root().knob('first_frame').value()
    comp_end = nuke.Root().knob('last_frame').value()
    project_dir = nuke.Root().knob('project_directory').getValue()
    if not os.path.exists(project_dir):
        project_dir = nuke.Root().knob('project_directory').evaluate()
    group_read_nodes = []


    #get selected write nodes
    sn = [n for n in nuke.selectedNodes() if n.Class() == "Write"]
    if sn != []:
        for n in sn:
            if n.knob('file') is not None:
                myfiletranslated, firstFrame, lastFrame, ismovie = evaluate_filepath_new(n.knob('file').getValue(), n.knob('file').evaluate(), project_dir)

                # get node data
                height = n.screenHeight()
                ndata = {
                            'filepath': myfiletranslated,
                            'ismovie' : ismovie,
                            'firstframe': firstFrame,
                            'lastframe': lastFrame,
                            'new_xpos': int(n.knob('xpos').value()),
                            'new_ypos': int(n.knob('ypos').value()) + height + 20,
                            'colorspace': n.knob('colorspace').getValue(),
                            'rawdata': n.knob('raw').value(),
                            'write_frame_mode': str(n.knob('frame_mode').value()),
                            'write_frame': n.knob('frame').value()
                            }
                group_read_nodes.append(ndata)

    # get  write nodes in groups
    # assumes only one write node in the group for now
    sn = [n for n in nuke.selectedNodes() if n.Class() == "Group"]
    if sn != []:
        for gn in sn:
            with gn:
                height = gn.screenHeight() # get group height and position
                new_xpos = int(gn.knob('xpos').value())
                new_ypos = int(n.knob('ypos').value()) + height + 20
                group_writes = [n for n in nuke.allNodes() if n.Class() == "Write"]
                if group_writes != []:
                    # there can be only 1 write node, taking first
                    n = group_writes[0]

                    if n.knob('file') is not None:
                        myfiletranslated, firstFrame, lastFrame, ismovie = evaluate_filepath_new(n.knob('file').getValue(), n.knob('file').evaluate(), project_dir)
                        # get node data
                        ndata = {
                                    'filepath': myfiletranslated,
                                    'ismovie' : ismovie,
                                    'firstframe': firstFrame,
                                    'lastframe': lastFrame,
                                    'new_xpos': new_xpos,
                                    'new_ypos': new_ypos,
                                    'colorspace': n.knob('colorspace').getValue(),
                                    'rawdata': n.knob('raw').value(),
                                    'write_frame_mode': str(n.knob('frame_mode').value()),
                                    'write_frame': n.knob('frame').value()
                                    }
                        group_read_nodes.append(ndata)

    # create reads in one go
    for oneread in group_read_nodes:
        # create read node
        create_read_node(oneread, comp_start)



def addToGui():
    import os
    script_path = os.path.normpath(os.path.realpath(os.path.abspath(__file__))).replace('\\', '/')
    script_dir = os.path.dirname(script_path)
    script_name = os.path.basename(script_path)
    script_name, script_extension = os.path.splitext(script_name)
    _c='/'.join(map(str, script_dir.split('script')[-1].split('/')[1:]))
    menupth = 'scripts/' + _c +'/'+ script_name
    nuke.menu("Nodes").addCommand(menupth, 'readFromWriteGroup.readFromWrite()', 'Shift+r')
